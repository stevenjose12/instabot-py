from __future__ import unicode_literals

import argparse
import os
import sys
import threading
import random
import time
import asyncio
import threading
import logging
from tqdm import tqdm
sys.path.append(os.path.join(sys.path[0], "../../"))
from instabot.bot.bot import Bot  # noqa: E402

# Listado de usuarios a etiquetar en los comentarios
usersList = [
	'anthony.deyvis',       'angie1294_',           'jessicamarin23',
	'gheorghelina',         'stepphiiandrea',       'oscartib',
]

commentsList = [
	'Excelentes productos muy buenos y recomendados ❤️',
	'Gran variedad en sus productos de maquillaje, recomendado al 100%',
	'Amo la marca de sus productos, recomendado al 100%',
	'Todos sus productos son muy hermosos😍❤️ y el servicio es excelente, recomendado al 100%',
	'No dejen de ir, son muy buenos y excelente calidad ❤️',
	'Originales y Hermosos 😍❤️',
	'😍❤️',
	'Excelentes',
	'Originales 100%',
	'No dejen de ir 😉',
	'🙏🙏🙏 Quiero ganar asi que siganlo 😁🙏🙏🙏'
]

credentials = [
	['username', 'PASSWORD'],
]

contests = [
	{
		'description': 'Concurso de Maquillaje',
		'url': 'https://www.instagram.com/p/CHJG1F1n34_/',
		'count_user': 2,
		'comments': commentsList,
		'users_comments': usersList,
		'count_comments': 0,
	},
	{
		'description': 'Concurso de Masajes',
		'url': 'https://www.instagram.com/p/CHJLH0QnDel/',
		'count_user': 3,
		'comments': None,
		'users_comments': usersList,
		'count_comments': 0,
	}
]


def comment(userLogin, contests=[]):
	bot = Bot(comment_delay=30)
	print('>>: userLogin', userLogin)
	logging.info('>>: userLogin ' + str(userLogin))
	list_times = [3, 5, 10, 15, 8, 7, 6, 20]
	if contests != []:
		if bot.login(username=userLogin[0], password=userLogin[1], is_threaded=True):
			print('>>: Login Success')
			logging.info('>>: Login Success')
			count = 0
			limit_comments = 100 * len(contests)
			while count <= limit_comments:
				for competition in contests:
					print("\n\n")
					print ('>>: to Comment > ' + userLogin[0] + '\ncomment Nº: '+str(competition['count_comments']+1)+'\nPost >> '+str(competition['description']))
					logging.info('>>: to Comment > ' + userLogin[0] + '\ncomment Nº: '+str(competition['count_comments']+1)+'\nPost >> '+str(competition['description']))
					print('>>: Comments: '+str(count)+'/'+str(limit_comments))
					users = random.sample(competition['users_comments'], competition['count_user'])
					list = []
					for user in users:
						list.append('@'+user)
						print('>>: userName > ', user)
						print('>>: List > ', list)
						pass
					to_comment = " ".join(list)
					to_comment = to_comment + ' '
					if competition['comments'] != None:
						commentText = random.sample(commentsList, 1)
						to_comment = to_comment + commentText[0]
						pass
					print('>>: to Comment > ', to_comment)
					logging.info('>>: to Comment > ' + to_comment)
					try:
						media_id = bot.get_media_id_from_link(competition['url'])
						bot.comment(media_id, to_comment)
						logging.info(">>: commented success > user: "+userLogin[0]+' in post > '+ competition['description'])
						print(">>: commented success > user: "+userLogin[0]+' in post > '+ competition['description'])
						competition['count_comments']+=1
						count += 1
						time.sleep(random.sample(list_times,1)[0])
						pass
					except:
						print("\n\n")
						print("Unexpected error:", sys.exc_info()[0])
						logging.info(">>: Error comment of user "+userLogin[0]+" in post >> "+competition['description'] + ", ERROR >> " + str(sys.exc_info()[0]))
						print(">>: Error comment of user "+userLogin[0]+" in post >> "+competition['description']+ ", ERROR >> "+str(sys.exc_info()[0]))
						pass
					pass
				pass
		else:
			print('>>>: ha ocurrido un error al iniciar sesion con: ' + userLogin[0])
			logging.info(
				'>>>: ha ocurrido un error al iniciar sesion con: ' + userLogin[0])
			pass
	else:
		return False


def main():
	for userLogin in credentials:
		# comment(userLogin)
		logging.info("Main    : before creating thread")
		x = threading.Thread(target=comment, args=(
			userLogin, contests,))
		logging.info("Main    : before running thread")
		x.start()
		print('>>: Before sleep')
		time.sleep(5)

main()
